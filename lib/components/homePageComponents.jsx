Home = React.createClass({
	mixins: [ReactMeteorData],
  	getMeteorData() {
    	var data = {
      		authInProcess: Meteor.loggingIn(),
      		canShow: !!Meteor.user(),
      		user: Meteor.user()
    	};
    	return data;
  	},
  	showContent(){
  		return(
  			<div>
				{this.data.canShow?  <AllSheets /> : <Landing /> }
			</div>
  		)
  	},
	render(){
		return(
			<div>
				{this.data.authInProcess?  <Progress /> : this.showContent() }
			</div>
		)
	}
})
Landing = React.createClass({
	render(){
		return(
			<div className="section">
				<div className="container">
					<div className="row center-align">
						<div className="col s12 m12">
							<img className="responsive-img" src="https://cf.dropboxstatic.com/static/images/index/animation-strips/posters/hero-poster-vfl1gZOAd.png" />
						</div>
					</div>
					<div className="row">
						<div className="col s12 m6" style={{color:'#37474f'}}>
							<h3>Save your time</h3>
						    <p className="flow-text">Collaborate with your classmates to create one master cheatsheet</p>
						</div>
						<Login />
					</div>
				</div>
			</div>
		)
	}
})
Login = React.createClass({
	loginWithGoogle: function(){
		console.log('Is user logging in ' + Meteor.loggingIn());
		Meteor.loginWithGoogle({
	      requestPermissions: ['email']
		},function(err){
			if (err) {
				console.log("Error login in with google " + err);	
			}
		});
	},
	render(){
		return(
			<div className="col s12 m6" style={{color:'#37474f'}}>
				<h3>Login to get started</h3>
				<p className="flow-text">When you login, you will be able to see all your sheet, create a sheet or continue working on the ones that are not completed yet.</p>
				<button ref="loginButton" className="waves-effect waves-light btn-large red accent-2 darken-2" type="submit" name="action" onClick={this.loginWithGoogle}><i className="fa fa-google"></i> Log in with Google</button>
			</div>
		)
	}
})
CreateDocButton = React.createClass({
	render(){
		return(
			<div className="col s12 m6" style={{color:'#37474f'}}>
				<button className="waves-effect waves-light btn-large green" type="submit" name="action"><i className="fa fa-plus"></i> Create Master Cheatsheet
				</button>
			</div>
		)
	}
})
Loader = React.createClass({
	render(){
		return(
			<div className="">
				<div className="row">
					<div className="col s6 offset-s6">
						<div className="valign-wrapper">
							<div className="preloader-wrapper active big valign" ref="loginLoader">
							    <div className="spinner-layer spinner-green-only">
							      <div className="circle-clipper left">
							        <div className="circle"></div>
							      </div><div className="gap-patch">
							        <div className="circle"></div>
							      </div><div className="circle-clipper right">
							        <div className="circle"></div>
							      </div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
})
Progress = React.createClass({
	render(){
		return(
		  	<div className="progress green lighten-4">
				<div className="indeterminate green"></div>
		  	</div>
		)
	}
})