MainLayout = React.createClass({
  render() {
    return (
    	<div className="grey lighten-5">
	        <header>
                <title>StudentHero</title>
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
                <link href="/css/widearea.min.css" rel="stylesheet" />

                <script type="text/javascript" src="/javascript/widearea.min.js"></script>
                
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            </header>
	        <main>
                <div>
                    <MainNavbar />
                    {this.props.content}
                </div>
            </main>
	        <footer id="contact">
            </footer>
        </div>
    )
  }
});
MainNavbar = React.createClass({
    mixins: [ReactMeteorData],
    getMeteorData() {
        var data = {
            authInProcess: Meteor.loggingIn(),
            canShow: !!Meteor.user(),
            user: Meteor.user()
        };

        console.log(data.user);
        return data;
    },
    componentDidMount : function(){
        $(".dropdown-button").dropdown({
            'hover':true,
            'belowOrigin':true
        });
        $(".button-collapse").sideNav();
    },
    componentDidUpdate : function(){
        $(".dropdown-button").dropdown({
            'hover':true,
            'belowOrigin':true
        });
        $(".button-collapse").sideNav();
    },
    logout(e){
        e.preventDefault();

        console.log("Logging out");
        Meteor.logout();
    },
    logIn(e){
        e.preventDefault();
        console.log('Is user logging in ' + Meteor.loggingIn());

        Meteor.loginWithGoogle({
          requestPermissions: ['email']
        },function(err){
            if (err) {
                console.log("Error login in with google " + err);   
            }
        });
    },
    getLoggedInButtons(){
        var profileImageUrl = this.data.user.profile.picture;
        profileImageUrl = profileImageUrl + "?sz=35";
        return(
            <div>
                <ul id="nav-mobile" className="right white hide-on-med-and-down">
                    <li className="nav-item waves-effect waves-light green"><a href="/suggestion">Feature Suggestion</a></li>
                    <li className="nav-item waves-effect waves-light"><a href="/"><i className="material-icons prefix green-text">view_module</i></a></li>
                    <li>
                        <a className="green-text dropdown-button valign-wrapper" href="#!" data-activates="dropdown1">
                            <img src={profileImageUrl} alt="" className="circle responsive-img valign" /> 
                             <i className="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                </ul>

                <ul id='dropdown1' className='dropdown-content'>
                    <li><a href='/profile' className="green-text">Profile</a></li>
                    <li className="divider"></li>
                    <li><a className="green-text" onClick={this.logout}>Logout</a></li>
                </ul>

                <ul className="side-nav" id="mobile-demo">
                    <li><a href="/" className="green-text">All Sheets</a></li>
                    <li><a href="/profile" className="green-text">Profile</a></li>
                    <li className="divider"></li>
                    <li><a className="green-text">Feature Suggestion</a></li>
                    <li className="divider"></li>
                    <li><a className="green-text" onClick={this.logout}>Logout</a></li>
                </ul>
            </div>
        )
    },
    getLoggedOutButtons(){
        return(
            <ul id="nav-mobile" className="right waves-effect waves-light white">
                <li className="nav-item" onClick={this.logIn}>
                    <a className="green-text">
                        <i className="fa fa-google left"></i> Log In With Google
                    </a>
                </li>
            </ul>
        )
    },
    render(){
        return(
            <nav className="white">
                <div className="nav-wrapper">
                    <a href="/" className="brand-logo left valign-wrapper" style={{paddingLeft:'20px'}}><img src="http://res.cloudinary.com/surebro/image/upload/c_scale,h_80/v1446274088/HeroIcon1024_phi1xq.png" className="responsive-img valign"/></a>
                    <a href="#" data-activates="mobile-demo" className="button-collapse green-text"><i className="material-icons">menu</i></a>
                    {this.data.canShow?  this.getLoggedInButtons() : this.getLoggedOutButtons() }
                </div>
            </nav>
        )
    }
});
NotFound = React.createClass({
  render() {
    return (
      <div className="grey lighten-5" style={{color:'#07353F',height:'100%', backgroundSize: 'cover',marginTop:'10px',paddingBottom:'20px'}}>
        <img className="center-block img-responsive" src="http://res.cloudinary.com/surebro/image/upload/v1444562107/404Lady_iagy4o.png" width="250px"/>
        <h1 className="center-align primary-text">I think you are lost</h1>
        <p className="flow-text center-align secondary-text" style={{fontSize:'200%'}}><span style={{display:'block'}}>Don't worry, I'll take you back home </span></p>
        <br />

        <div className="row">
            <div className="center-align">
                <a href="/" type="button" className="btn red accent-2">Take me Home</a>
            </div>
        </div>
      </div>
    );
  }
});
