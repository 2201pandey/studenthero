AcceptingInvite = React.createClass({
	mixins:[ReactMeteorData],
	getInitialState() {
    	return {
      		pendingInvite: {},
      		isProcessing: true,
      		message: ''
    	}
  	},
	getMeteorData(){
		var data = {};
		var handle = Meteor.subscribe('SinglePendingInvite', this.props.pendingInviteId);
		console.log("Pending Invite id : " + this.props.pendingInviteId);
		if(!!Meteor.user()){
			data.pendingInvite = PendingInvites.findOne(this.props.pendingInviteId);
			if(data.pendingInvite){
				console.log('Invite meant for user ' + data.pendingInvite.userEmail);

				Meteor.call('acceptInvitation', this.props.pendingInviteId, function(error, returnedSheetId){
					if(! error){
						//Go to the sheet
						Materialize.toast('Invitation Accepted', 4000) 
						var params = {
							sheetId: returnedSheetId
						};
						FlowRouter.go(FlowRouter.path('SheetDetail', params));
					}else{
						console.log(error.reason);
						data.isProcessing = false;
						data.message = error.reason;
					}
				});
			}else{
				//Invite does not exists
				data.message = "Dude, Your Invite was not found. Maybe they canceled your invitation. Sucks";
				data.isProcessing = false;
			}
		}else{
			//User not logged in
			data.message = "Ummmm... this is awkward but I don't know you. Could you login and try again?"
			data.isProcessing = false;
		}
		
		console.log("Returning data " + data.isProcessing + '   ' + data.message);
	    return data;  		
	},
	render(){
		return(
			<div>
				{this.data.isProcessing ?  <Progress key={this.props.pendingInvite} /> : null}
				<MessageHeader key={this.props.pendingInviteId} text={this.data.message} />
			</div>
		)
	}
});
MessageHeader = React.createClass({
	render(){
		return(
			<div className="center-align container">
				<div className="row">
					<div className="col s12">
						<h3 className="secondary-text">{this.props.text}</h3>
					</div>
				</div>
			</div>
		)
	}
})