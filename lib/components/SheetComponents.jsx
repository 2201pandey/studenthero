SheetLayout = React.createClass({

  	render() {
    	return (
	    		<div className="white">
		        <header>
	                <title>Your Sheets</title>
	                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	                <link href="/css/widearea.min.css" rel="stylesheet" />

	                <script type="text/javascript" src="/javascript/widearea.min.js"></script>
	                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	            </header>
		        <main>
	                <div>
	                    <MainNavbar />
	                    {this.props.content}
	                </div>
	            </main>
		        <footer id="contact">
	            	
	            </footer>
	        </div>
	    )
	}
});
AllSheets = React.createClass({
	mixins: [ReactMeteorData],
	getInitialState() {
    	return {
      		sheets: [],
      		isLoading : true
    	}
  	},
	getMeteorData() {
		var data = {};
		var handle = Meteor.subscribe('allSheets');
		if(handle.ready()){
			data.sheets = Sheets.find({users: Meteor.userId()}, {sort: {createdAt : 1}}).fetch();
			data.isLoading = false;
		}
	    return data;
	 },
	renderSheets(){
		return (this.data.sheets || []).map(function(sheet){
			return <SheetPreview key={sheet._id} sheet={sheet} />
		});
	},
	showContent(){
		return(
			<div>
				{this.renderSheets()}
				<NewSheetPreview />
			</div>
		)
	},
	render(){
		return(
			<div>
				<div className="container">
					<p className="secondary-text">Your Sheets</p>
					<div className="row">
						{this.data.isLoading?  <Progress /> : this.showContent() }
					</div>
					<FloatingCreateSheetButton />
				</div>
			</div>
		)
	}
})
FloatingCreateSheetButton = React.createClass({
	componentDidMount: function(e){
		$('.tooltipped').tooltip({delay: 100});
	},
	createSheet(e){
		e.preventDefault();

		Meteor.call("createNewSheet", function(err, data){
			if(! err){
				console.log('new sheet id : ' + data);
				Materialize.toast('Sheet created', 3000); 

				var params = {
					sheetId: data
				};
				var path = FlowRouter.path('SheetDetail', params);
				FlowRouter.go(path);
			}
		});
	},
	render(){
		return(
			<div className="fixed-action-btn" style={{bottom:'45px',right:'24px'}}>
			    <a className="btn-floating btn-large waves-effect waves-light red tooltipped" data-position="left" data-delay="100" data-tooltip="Create a Sheet" onClick={this.createSheet}><i className="material-icons">add</i></a>
			</div>
		)
	}
})
SheetDetail = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		var data = {};
		var sheetHandle = Meteor.subscribe('singleSheet', this.props.sheetId);
		var sectionHandle = Meteor.subscribe('UsersSectionForSheet', this.props.sheetId);

		data.sheet = Sheets.findOne(this.props.sheetId);
		
		var sectionArrays =  Sections.find({ $and: [{
													sheet:this.props.sheetId, 
													userId:Meteor.userId()
												}]
										}).fetch();
		if(sectionArrays.length > 0){
			data.section = sectionArrays[0];
		}

		console.log("Section " + data.section);

		data.isLoading = !(sheetHandle.ready() && sectionHandle.ready());
	    return data; 		
	},
	showContent(){
		if(this.data.isLoading){
			return(
				<Progress />
			)
		}
		var isUserAdmin = false;
		if(this.data.sheet.userId == Meteor.userId()){
			isUserAdmin = true;
		}
		return(
			<div>
				<div className="">
					<SheetToolBar key={this.data.sheet._id} sheet={this.data.sheet} isAdmin={isUserAdmin}/>
				</div>
				<div className="row" style={{marginTop:'-40px'}}>
					<div className="white col s12 m8 l9" style={{height:'100%'}}>
						<Section section = {this.data.section}/>
					</div>
					<div className="col s12 m4 l3 grey lighten-4" style={{height:'100%',paddingBottom:'60px'}}>
						<CollaboratorsActivitySection key={this.data.sheet._id} sheetId={this.data.sheet._id} canRemoveCollab={isUserAdmin}/>
					</div>
				</div>
			</div>
		)
	},
	render(){
		return(
			<div className="">
				{this.data.isLoading?  <Progress /> : this.showContent() }
			</div>
		)
	}
})
NewSheetPreview = React.createClass({
	createSheet(e){
		e.preventDefault();

		Meteor.call("createNewSheet", function(err, data){
			if(! err){
				console.log('new sheet id : ' + data);
				Materialize.toast('Sheet created', 3000); 

				var params = {
					sheetId: data
				};
				var path = FlowRouter.path('SheetDetail', params);
				FlowRouter.go(path);
			}
		});
	},
	render(){
		return(
			<div>
				<div className="col s12 m4 l3 waves-effect waves-light" onClick={this.createSheet}>
			        <div className="card small white">
			            <div className="card-content grey-text center-align">
			            	<span className="card-title red-text accent-2">New Sheet</span>
			            	<br />
			              	<h1 className="red-text accent-2">+</h1>
			            </div>
			        </div>
			    </div>
			</div>
		)
	}
})
SheetPreview = React.createClass({
	goToDetail(){
		var params = {
			sheetId:this.props.sheet._id
		};
		var path = FlowRouter.path('SheetDetail', params);
		FlowRouter.go(path);
	},
	render(){
		return(
			<div className="col s12 m4 l3">
		        <div className="card small white">
		        	<span className="card-title truncate" style={{color:'#212121',padding:'20px'}}>{this.props.sheet.name}</span>
		            <div className="card-content secondary-text" >
		              <p>Snippet Snippet Snippet Snippet Snippet Snippet Snippet Snippet </p>
		            </div>
		            <div className="card-action">
		              <a className="red-text text-accent-3" onClick={this.goToDetail} style={{cursor:'pointer'}}>More...</a>
		            </div>
		            
		        </div>
		    </div>
		)
	}
})
SheetToolBar = React.createClass({
	mixins: [React.addons.LinkedStateMixin],
	getInitialState: function() {
        return {
            name: this.props.sheet.name
        }
    },
    componentDidMount: function(e){
		$('.modal-trigger').leanModal({
			dismissible: true, // Modal can be dismissed by clicking outside of the modal
			complete: function(){
				$('.lean-overlay').remove();
			}
		});
	},
	renderCollabs(){
		return(
			<p>All Collaborators</p>
		)
	},
	handleNameChange(e){
		e.preventDefault();

		var newSheetName = React.findDOMNode(this.refs.sheetNameInput).value;
		this.props.sheet.name = newSheetName;
	},
	handleNameInputBlur(e){
		e.preventDefault();

		var newSheetName = React.findDOMNode(this.refs.sheetNameInput).value;
		Meteor.call('updateSheetName', this.props.sheet._id, newSheetName);
	},
	handleDeleteButtonClicked(e){
		e.preventDefault();

		$('#deleteConfirmationModal').openModal();
	},
	handleCancelButtonClicked(e){
		e.preventDefault();

		$('#deleteConfirmationModal').closeModal();
		$('.lean-overlay').remove();
	},
	handleDeleteSheet(e){
		e.preventDefault();

		Meteor.call('deleteSheet', this.props.sheet._id);

		Materialize.toast("Sheet deleted forever", 3000); 
		FlowRouter.go('Home');
	},
	generatePDF(e){
		e.preventDefault();

		Meteor.call('generatePdf', this.props.sheet._id, function(error, data){
			if(error){

			}else{
				console.log("PDF url is " + data);
				var win = window.open(data, '_blank');
  				win.focus();
			}
		});
	},
	render(){
		var isDisabled = "";
		if(! this.props.isAdmin){
			isDisabled = "disabled";
		}
		return(
			<div className="row">
				<div className="blue-grey darken-2 white-text valign-wrapper" style={{borderBottom:'1px solid #bdbdbd',padding:'10px'}}> 
					<div className="col s12 m4 valign" style={{paddingLeft:'10px'}}>
						<div className="input-field col s12">
				        	<input onBlur={this.handleNameInputBlur} valueLink={this.linkState('name')} placeholder="Sheet Name" id="inputSheetName" type="text" className="sheetNameInput white-input" ref="sheetNameInput" style={{fontSize:'200%'}} disabled={isDisabled}/>
				        </div>
					</div>
					<div className="col s12 m8 l8 valign">
						<div className="actionButtons right">
							{
								this.props.isAdmin && 
								<button data-target="deleteConfirmationModal" className="btn waves-effect waves-red waves-light blue-grey darken-3 modal-trigger" style={{margin:'10px'}}>Delete this Sheet</button>
							}
							<button className="btn waves-effect waves-light green white-text" style={{margin:'10px'}} onClick={this.generatePDF}>Preview PDF</button>
			        		<button className="btn waves-effect waves-light green white-text" style={{margin:'10px'}}>Download PDF</button>
			        	</div>
					</div>
				</div>
				<div id="deleteConfirmationModal" className="modal">
			    	<div className="modal-content">
			      		<h4 className="primary-text">Are you sure you want to delete this sheet?</h4>
			      		<p className="secondary-text">All your contribution to the sheet will be destroyed forever. Your collaborators will lose all their work and you will not be able to recover it back. We suggest that you back it up first. Download the PDF if you want.</p>
			    	</div>
			    	<div className="modal-footer">
			      		<button className="btn waves-effect waves-light red accent-2" style={{margin:'10px'}} onClick={this.handleDeleteSheet}>Delete</button>
			      		<button className="btn waves-effect waves-light grey lighten-5 grey-text" style={{margin:'10px'}} onClick={this.handleCancelButtonClicked}>Cancel</button>
			    	</div>
				</div>
			</div>
		)
	}
})
Section = React.createClass({
	mixins:[React.addons.LinkedStateMixin],
	getInitialState: function() {
        return {
            text: this.props.section.text
        }
    },
	componentDidMount: function(e){
		// wideArea().setOptions({ defaultColorScheme: "dark", closeIconLabel: "Exit" });
		$('#sectionTextArea').autosize();
	},
	componentDidUpdate: function(e){
		var newSectionText = this.state.text;
		Meteor.call('updateSectionText', this.props.section._id, newSectionText);
	},
	handleTextInputBlur(e){
		e.preventDefault();

		var newSectionText = e.target.value;
		Meteor.call('updateSectionText', this.props.section._id, newSectionText);
	},
	handleTextInputChange(e){
		e.preventDefault();

		var newSectionText = e.target.value;
		Meteor.call('updateSectionText', this.props.section._id, newSectionText);
	},
	render(){
		return(
			<div className="" style={{padding:'20px'}}>
				<textarea onBlur={this.handleTextInputBlur} className="section-editor flow-text black-text" valueLink={this.linkState('text')} id="sectionTextArea" ref="sectionTextArea" data-widearea="enable" placeholder="This is your space. Start typing and contributing to the sheet"/> 
			</div>
		)
	}
})
CollaboratorsActivitySection = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		var data = {};
		var handle = Meteor.subscribe('allSectionsForSheet', this.props.sheetId);
		data.isLoading = !handle.ready();

		if(handle.ready()){
			data.sections = Sections.find({sheet:this.props.sheetId}).fetch();
		}
	    return data; 		
	},
	renderSections(){
		var canRemoveCollab = this.props.canRemoveCollab;
		if(! this.data.isLoading){
			if(this.data.sections.length > 1){
				return this.data.sections.map(function(section){
					if(section.userId != Meteor.userId()){
						return (
							<div className="box" style={{paddingTop:'20px'}}>
								<HeaderDetailRow key={section.userId} detail={section.snippet} userName={section.user.name} userImage={section.user.imageUrl} canRemoveCollab={canRemoveCollab}/>
							</div>
						)
					}
				}); 	
			}else{
				console.log('rendering Empty sections');
				return(
				 	<p className="flow-text secondary-text">You are working on this sheet all alone. Inviting just one person reduces your workload by 50%. Do it.</p>
				);
			}
		}else{
			return(
				<Progress />
			)
		}
	},
	render(){
		return(
			<div className="">
				<div className="" style={{padding:'20px'}}>
					<div className="col s12">
						<h6 className="secondary-text center-align">
							People that are collaborating to build this sheet
						</h6>
					</div>
					<div className="col s12 center-align" style={{padding:'20px'}}>
						<InviteButton key={this.props.sheetId} sheetId={this.props.sheetId} />
					</div>
					<div className="col s12 left-align" style={{padding:'20px'}}>
						{this.renderSections()}
					</div>
				</div>				
			</div>
		)
	}
})