InviteButton = React.createClass({
	componentDidMount: function(e){
		$('.modal-trigger').leanModal({
			dismissible: true, // Modal can be dismissed by clicking outside of the modal
			complete: function(){
				$('.lean-overlay').remove();
			}
		});
	},
	handleInviteButtonClicked(e){
		e.preventDefault();
		console.log('Invite button clicked');

		$('#inviteModal').openModal();
	},
	handleEmailChange(e){
		e.preventDefault();

		var email = React.findDOMNode(this.refs.emailInput).value.trim();
	},
	handleSendInvite(e){
		e.preventDefault();

		var email = React.findDOMNode(this.refs.emailInput).value.trim();
		Meteor.call('inviteUserToSheet', email, this.props.sheetId);

		Materialize.toast("Invitation sent", 3000); 
	},
	render(){
		return(
			<div>
				<button data-target="inviteModal" className="waves-effect waves-light waves-red btn red accent-2 modal-trigger">Invite Collaborator</button>

				<div id="inviteModal" className="modal bottom-sheet">
				    <div className="modal-content">
				      <h4>Enter an email</h4>
				      <p className="secondary-text">We will send an email to this guy so they can accept your invitation</p>
				      <input placeholder="Email" id="inputEmail" type="text" className="emailInput" ref="emailInput" onChange={this.handleEmailChange}/>
				    </div>
				    <div className="modal-footer">
				      <a href="#!" className="modal-action modal-close waves-effect red accent-2 btn-large" onClick={this.handleSendInvite}>Invite</a>
				    </div>
				</div>
			</div>
		)
	}
})
HeaderDetailRow = React.createClass({
	render(){
		var imageUrl = this.props.userImage;
		imageUrl = imageUrl + "?sz=35";
		return(
			<div className="">
				<div className="row valign-wrapper">
					<img className="circle responsive-img valign" src={imageUrl} />
					<span className="primary-text valign" style={{marginLeft:'10px'}}>{this.props.userName}</span>
				</div>
				<div className="">
					<p className="secondary-text" style={{wordWrap:"break-word"}}>{this.props.detail}</p>
				</div>
			</div>
		)
	}
})