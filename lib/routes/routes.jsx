if (Meteor.isClient) {
	//Route Groups
	var sheetsSection = FlowRouter.group({
    	prefix: "/sheets",
    	triggersEnter: [function(context, redirect) {
	        if(Meteor.loggingIn() || Meteor.userId()){

	        }else{
	        	FlowRouter.go('Home');
	        }
	    }]
	});
	var profileSection = FlowRouter.group({
    	prefix: "/profile",
    	triggersEnter: [function(context, redirect) {
	        if(Meteor.loggingIn() || Meteor.userId()){

	        }else{
	        	FlowRouter.go('Home');
	        }
	    }]
	});

	//Routes

	//Home
	FlowRouter.route( '/', {
	  action: function() {
	    // Do whatever we need to do when we visit http://app.com/terms.
	    console.log( "Okay, we're on the Home page" );
	    ReactLayout.render(MainLayout, {content: <Home />});
	  },
	  name: 'Home' // Optional route name.
	});

	//Sheet
	sheetsSection.route('/', {
    	action: function() {
    		ReactLayout.render(SheetLayout, {content: <AllSheets />});
    	},
    	name: 'AllSheets' 
	});
	sheetsSection.route('/new', {
		action: function(){
			ReactLayout.render(SheetLayout, {content: <NewSheet />});
		},
		name: 'NewSheet'
	});
	sheetsSection.route('/:sheetId', {
		subscriptions: function(params, queryParams) {
        	this.register('Sheet', Meteor.subscribe('singleSheet', params.sheetId));
    	},
		action: function(params){
			ReactLayout.render(SheetLayout, {content: <SheetDetail sheetId={params.sheetId} />, sheetId: params.sheetId});
		},
		name: 'SheetDetail'
	});

	//Profile
	profileSection.route('/', {
		name: 'Profile'
	})

	//Invites
	FlowRouter.route( '/accept/:pendingInviteId', {
	  action: function(params) {
	    // Do whatever we need to do when we visit http://app.com/terms.
	    console.log( "Trying to accept invitation for PI with Id " + params.pendingInviteId);
	    ReactLayout.render(MainLayout, {content: <AcceptingInvite pendingInviteId={params.pendingInviteId}/>});
	  },
	  name: 'PendingInvites' // Optional route name.
	});

	//Exceptions
	FlowRouter.notFound = {
	    action() {
	    	console.log("Cant Find page");
	    	ReactLayout.render(MainLayout, {content: <NotFound />});
	    }
	};
}