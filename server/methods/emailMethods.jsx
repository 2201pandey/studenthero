Meteor.methods({
	sendActionEmail: function(to, subject, heading, message, buttonText, buttonUrl, secondMessage){

		PrettyEmail.send ('call-to-action',{
		  	to: to,
		  	subject: subject,
		  	heading: heading,
		  	message: message,
		  	buttonText: buttonText,
		  	buttonUrl: buttonUrl,
		  	messageAfterButton: secondMessage
		});
	}
});