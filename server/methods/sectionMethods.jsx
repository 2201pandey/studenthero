Meteor.methods({
	createNewSection: function(sheetId){
		if(Meteor.userId()){
			var sheet = Meteor.call('getSheet', sheetId);
			if(sheet){
				var user = Meteor.users.findOne(Meteor.userId());
				var sectionId = Sections.insert({
			      	userId: Meteor.userId(),
			      	sheet: sheetId,
			      	text: '',
			      	snippet:'',
			      	user:{
			      		name: user.profile.firstName,
			      		imageUrl: user.profile.picture
			      	},
			      	createdAt: new Date()
			    });
		    	console.log("Section created Id : " + sectionId);

		    	return sectionId;
			}else{
				throw new Meteor.Error("SheetNotFound", "This sheet does not exists. Maybe it was deleted");	
			}
		}else{
			throw new Meteor.Error("UserNotLoggedIn", "Please log in first");
		}
	},
	getAllSectionForSheet: function(sheetId){
		if(Meteor.userId()){
			var sheet = Meteor.call('getSheet', sheetId);
			if(sheet){
				return Sections.find({'sheet':sheetId}).fetch();
			}else{
				throw new Meteor.Error("SheetNotFound", "This sheet does not exists. Maybe it was deleted");	
			}
		}else{
			throw new Meteor.Error("UserNotLoggedIn", "Please log in first");
		}
	},
	updateSectionText: function(_id, text){
		if(Meteor.userId()){
			var section = Sections.find({'_id':_id}).fetch();
			if(section){
				var snippet =  text;
				if(snippet.length > 125){
					snippet = snippet.substr(snippet.length - 125);
					snippet = "..." + snippet;
				}

				Sections.update(_id, {$set: {text: text}});
				Sections.update(_id, {$set: {snippet: snippet}});
			}else{
				throw new Meteor.Error('SectionNotFound', 'Are your sure you are invited to this sheet?');
			}
		}
	}
});