Meteor.startup(function () {
	Meteor.methods({
		inviteUserToSheet: function(userEmail, sheetId){
			//Check if user exists

			//Check if sheet exists
			var sheet = Sheets.findOne(sheetId);
			if(sheet){
				//Add data to PendingInvites
				if(Meteor.userId()){

					var pendingInviteId = PendingInvites.insert({
				      	userEmail: userEmail,
				      	sheetId: sheetId,
				      	dealtWith: false,
				      	createdAt: new Date()
				    });

					//Send Email to user with link /accept/:pendingInviteId/:sheetId. Sheet Id so user can be redirected to the sheet	    
					var heading = "You have been summoned";
					var messageAfterButton = "You can choose to decline this invitation but that would make " + Meteor.user().profile.name + " very sad :( To decline, ignore this email.";
					var message = "Hey, you have been invited by <b>" + Meteor.user().profile.name + "</b> to collaborate to build <b>" + sheet.name +"</b>. If you accept it, you will have access to a merged version of this sheet with everybody's contribution. Wanna Accept?";
					var subject = "Invitation from " + Meteor.user().profile.name;

					var host = this.connection.httpHeaders.host;
					var path = "/accept/"+pendingInviteId;
					var acceptInvitationUrl = "http://" + host + path;

					console.log('Pending invitation accept url ' + acceptInvitationUrl);

					Meteor.call('sendActionEmail', userEmail, subject, heading, message, "Accept the Invitation", acceptInvitationUrl ,messageAfterButton);
				}
				
			}else{
				//Return error
			}
		},
		acceptInvitation: function(pendingInviteId){
			//Check if pendingInvite record exists
			if(! Meteor.userId){
				throw new Meteor.Error("UserLoginError", "Ummm... You need to log in before accepting an invitation");
			}
			var invite = PendingInvites.findOne(pendingInviteId);
			if(invite){
				//Check if Invite has already been updated
				if(! invite.dealtWith){
					//Find Sheet
					var sheet = Sheets.findOne(invite.sheetId);
					if(sheet){
						var user = Meteor.user();

						//Add user to sheets collaborator
						Sheets.update(sheet._id, {$push: {users: user._id}});

						//Add a section for this user
						Meteor.call('createNewSection', sheet._id);

						//Set invite dealt with
						PendingInvites.update(invite._id, {$set: {dealtWith: true}});

						//return success
						return sheet._id;
					}else{
						//Throw error. Sheet does not exists. Maybe it got deleted
						throw new Meteor.Error("Sheet not Found", "Looks like the sheet does not exists.");
					}
				}else{
					//Throw error. This invitation has already been accepted
					throw new Meteor.Error("Invitation Invalid", "This invitation has already been accepted");
				}
			}else{
				//Throw error. Invitation does not exists
				throw new Meteor.Error("Invitation not found", "This invitation is not valid. You have been punked");
			}
		}
	})
})