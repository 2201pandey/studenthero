Meteor.methods({
	createNewSheet: function(){
		if(Meteor.userId()){
			var sheetId = Sheets.insert({
				name: "Untitled Sheet",
		      	userId: Meteor.userId(),
		      	users:[Meteor.userId()],
		      	createdAt: new Date()
		    });
		    console.log("Created sheet with Id : " + sheetId);

		    // Users.update(Meteor.userId(), {$push: {sheets: sheetId, role: "Owner"}})

			//Create a section for this sheet and user
			Meteor.call('createNewSection', sheetId);
			
		    return sheetId;
		}else{
			FlowRouter.go('Home');
		}
	},
	insertSheet: function(sheetName){
		if(Meteor.userId()){
			return Sheets.insert({
		      name: sheetName,
		      userId: Meteor.userId(),
		      users:[userId],
		      createdAt: new Date()
		    });
		}else{
			FlowRouter.go('Home');
		}
	},
	updateSheetName: function(_id, sheetName){
		if(Meteor.userId()){
			var sheet = Sheets.findOne(_id);
			if(sheet.userId != Meteor.userId()){
				//Return error
				throw new Meteor.Error("Updating Sheet Name", "You cannot update a sheet's name because you don't own this sheet");
			}else{
				Sheets.update(_id, {$set: {name: sheetName}});
			}
		}
	},
	getSheets: function(){
		/*
		"users": {
        	"$in": producerIds
    	}
		*/
		return Sheets.find({'users': Meteor.userId()}, {sort: {createdAt : 1}}).fetch()
	},
	getSheet: function(sheetId){
		var sheet = Sheets.findOne(sheetId);
		return sheet;
	},
	deleteSheet: function(sheetId){
		var sheet = Sheets.findOne(sheetId);
		if(sheet){	
			if(sheet.userId == Meteor.userId()){
				Sheets.remove(sheetId);
			}else{
				throw new Meteor.Error("UnAuthorized Action", "Do you know deleting somebody else's sheet is punishable by law? Actually it's not but c'mon, be nice.");
			}
		}else{
			throw new Meteor.Error("SheetNotFound", "The sheet that you are trying to delete was not found. Maybe you just thinking about deleting the sheet actually deleted it. Woad superpowers.");
		}
	},
	generatePdf: function(sheetId){
		var sheet = Sheets.findOne(sheetId);
		var user = Meteor.users.findOne(Meteor.userId());

		if(sheet){
			var sections = Sections.find({'sheet':sheetId}).fetch();
			var pdfText = '';
			//Generate PDF
			var paragraphSettings = {
				align: 'left',
				columns: 2,
				columnGap: 10
			};
			var headerSettings = {
				align: 'center',
				columns: 1,
				underline: true
			}
			var docInfo = {
				Title: "Title",
				Author: "Chalamphong Pandey"
			};
			var docName = sheet.name.split(' ').join('_');

			var doc = new PDFDocument({size: 'A4', margin: 15});
			doc.info = docInfo;
			doc.fontSize(15);
			doc.font('Helvetica');
			doc.text(sheet.name+'(Cheatsheet)', headerSettings);

			doc.fontSize(10);
			doc.moveDown();
			for(var i=0;i<sections.length;i++){
				var section = sections[i];
				var sectionText = section.text;

				pdfText = pdfText + sectionText;
			}

			doc.text(pdfText, paragraphSettings);
 			doc.writeSync(process.env.PWD + '/public/pdf/'+docName+'.pdf');

 			var urlToDoc = '/pdf/'+docName+'.pdf';
 			return urlToDoc;
		}else{	
			throw new Meteor.Error("SheetNotFound", "The sheet that you are trying generate a PDF for was not found. Maybe the owner deleted it.")
		}
	}
})