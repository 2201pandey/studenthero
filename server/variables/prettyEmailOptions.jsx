Meteor.startup(function () {
	PrettyEmail.options = {
	  from: 'Surebro <yourfriends@surebro.com>',
	  logoUrl: 'http://res.cloudinary.com/surebro/image/upload/v1444899161/Icon-60_3x_zbfjky.png',
	  companyName: 'Surebro',
	  companyUrl: 'http://surebro.com',
	  companyEmail: 'yourfriends@surebro.com',
	  siteName: 'StudentHero',
	  twitter:'https://twitter.com/SureBroCorp',
	  email: 'pr@surebro.com',
	  showFooter: true,
	  showFollowBlock: true
	};

	PrettyEmail.style ={
		fontFamily: 'Helvetica',
		textColor: '#424242',
		buttonColor: '#FFFFFF',
		buttonBgColor: '#ff5252'
	};
})