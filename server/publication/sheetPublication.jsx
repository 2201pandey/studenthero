  Meteor.publish("allSheets", function () {
    return Sheets.find({users: this.userId});
  });
  Meteor.publish("singleSheet", function (sheetId) {
    return Sheets.find({_id: sheetId});
  });
