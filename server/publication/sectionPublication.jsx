Meteor.publish("allSectionsForSheet", function (sheetId) {
    return Sections.find({sheet: sheetId});
  });
Meteor.publish("UsersSectionForSheet", function (sheetId) {
	var trigger = Sections.find({ $and: [{
		sheet:sheetId, 
		userId:this.userId
	}]});
	return trigger;
});
Meteor.publish("updateSectionText", function(_id, text){
	Sections.update(_id, {$set: {text: text}});
});